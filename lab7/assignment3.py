import cv2 as cv
import numpy as np

def imshow(title, img):
  cv.imshow(title, img)
  cv.waitKey(0)
  cv.destroyAllWindows()

def subsample(channel, bit_depth):
  '''
  Quantize a channel's colors to a certain bit depth.
  For instance, 8 bits allows for 256 colors, etc.
  Note: This function takes A SINGLE CHANNEL, not the whole image as input.
  '''
  div = pow(2, bit_depth)
  subsampled = channel // div * div + div // 2
  return subsampled

img = cv.imread("images/lenna.png")
imshow("Original", img)

## --- BGR ---

# subsample BGR image blue (0), green (1) and red (2) to a bit depth of 7
bgr_bit_depth = 7
img_rgb = img.copy()
img_rgb[:, :, 0] = subsample(img_rgb[:, :, 0], bgr_bit_depth)
img_rgb[:, :, 1] = subsample(img_rgb[:, :, 1], bgr_bit_depth)
img_rgb[:, :, 2] = subsample(img_rgb[:, :, 2], bgr_bit_depth)

imshow("RGB subsampled", img_rgb)
cv.imwrite("images/rbg_subsampled.png", img_rgb)

# (width * height) * 3 channels * 7 bits
img_size = (img.shape[0] * img.shape[1]) * 3 * bgr_bit_depth
print(f'RGB subsampled size:\t{img_size} bits')

## --- YCbCr ---

img_ycbcr = cv.cvtColor(img, cv.COLOR_BGR2YCrCb)

# TODO: Subsample the Cb and Cr channel to a bit depth of 6, while leaving Y unsampled.
# Use the code above as a guide.
cbcr_bit_depth = 6
img_ycbcr[:, :, 1] = subsample(img_ycbcr[:, :, 1], cbcr_bit_depth)
img_ycbcr[:, :, 2] = subsample(img_ycbcr[:, :, 2], cbcr_bit_depth)



img_ycbcr = cv.cvtColor(img_ycbcr, cv.COLOR_YCrCb2BGR)
imshow("YCbCr subsampled", img_ycbcr)
cv.imwrite("images/ycbcr_subsampled.png", img_ycbcr)

# TODO: Calculate size of image in bits and print it out. (See the BGR calculation above as guide.)
# Keep in mind, Y takes up up 8 bits per pixel, while Cb and Cr take up 7.

img_size = (img.shape[0] * img.shape[1]) * (2 * cbcr_bit_depth + 1 * 8)
print(f'YCbCr subsampled size:\t{img_size} bits')



# TODO: Note the differences in the two image's sizes and visible detail.
# Which image has more detail? Why do you think that is? 
# (Write your answer in a comment below.)
# Vidimo da ycrcb slika zauzima manje memorije, dok pri tome ima vise detalja
# ali je boja malo drukcija.