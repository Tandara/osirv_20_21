import numpy as np
import cv2
from matplotlib import pyplot as plt
import random as rng

def imshow(name, img):
  cv2.imshow(name, img)
  cv2.waitKey(0)
  cv2.destroyAllWindows()

def threshold_otsu(img):
  # TODO: Threshold img with BINARY INVERSE AND OTSU and return thresholded image
  # help: https://docs.opencv.org/master/d7/d4d/tutorial_py_thresholding.html
  ret,th = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
  return th

def clahe(img):
  # TODO: Apply Contrast Limited Adaptive Histogram Equalization (CLAHE) to img and return it.
  # use a clip limit of 2 and a tile grid size of 8x8
  # help: https://docs.opencv.org/master/d5/daf/tutorial_py_histogram_equalization.html
  clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
  img = clahe.apply(img)
  return img


def distance_transform(img):
  # TODO: Apply a distance transform to img and return it.
  # use L2 for the distance type and a mask size of 3
  # help: https://docs.opencv.org/3.4/d7/d1b/group__imgproc__misc.html#ga25c259e7e2fa2ac70de4606ea800f12f
  # check the readme for instructions on how to read the docs
  img = cv2.distanceTransform(img, cv2.DIST_L2, 3)
  return img


# Load the source image
img = cv2.imread('images/cells.jpg')
# Convert image to grayscale
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
# Apply CLAHE
# gray = clahe(gray)
# OTSU threshold the image
thresh = threshold_otsu(gray)

cv2.imwrite("images/thresholded.png", thresh)
imshow("Thresholded image", thresh)

# Use morphological processing to remove noise from the image
# more info: https://docs.opencv.org/3.4/d9/d61/tutorial_py_morphological_ops.html
kernel = np.ones((3,3),np.uint8)
opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations = 2)

# Finding sure background area
sure_bg = cv2.dilate(opening,kernel,iterations=3)

# Apply distance transform the image
dist_transform = distance_transform(opening)
# Normalize so that the distance transform image has minimum and maximum values from 0 to 1
cv2.normalize(dist_transform, dist_transform, 0, 1.0, cv2.NORM_MINMAX)

cv2.imwrite("images/distance_transform.png", dist_transform * 255)
imshow("Distance Transform", dist_transform)

# Sure foreground area is where the distance transform has the highest values,
# so threshold the image so that every pixel below 0.2 is set to 0
ret, sure_fg = cv2.threshold(dist_transform, 0.2*dist_transform.max(), 255, 0)

# Finding unknown region, i.e. the difference between sure foreground and sure background (the edges of the cells)
sure_fg = np.uint8(sure_fg)
unknown = cv2.subtract(sure_bg,sure_fg)

# Find areas of connected pixels - we call these markers
ret, markers = cv2.connectedComponents(sure_fg)
# Add one to all markers so that sure background is not 0, but 1
markers = markers+1
# Now, mark the region of unknown with zero
markers[unknown==255] = 0

# Finally, apply watershed to the image using the markers as guides
markers = cv2.watershed(img, markers)

# Generate random colors
colors = []
for i in range(markers.shape[0] * markers.shape[1]):
  colors.append((rng.randint(0,256), rng.randint(0,256), rng.randint(0,256)))

# Create the result image
dst = np.zeros((markers.shape[0], markers.shape[1], 3), dtype=np.uint8)
# Fill labeled objects with random colors
for i in range(markers.shape[0]):
    for j in range(markers.shape[1]):
        index = markers[i,j]
        dst[i,j,:] = colors[index-1]

imshow("Result", dst)
cv2.imwrite("images/result_original.png", dst)

# Kada se ne korisiti clahe, dolazi do pretjerane segmentacije na slici, a
# kada je ukljucen, poboljsava se kontrast na slici, odnosno bolje su naglaseni rubovi 
# krvnih stanica, sto pridonosi boljem radu watershed algoritma na slici.