naziv = ""
ulice = []
while naziv != "prekid":
    naziv = input("Unesite naziv ulice: ")
    if len(naziv) > 6 and len(naziv) < 16:
        ulice.append(naziv)

max_slova = len(ulice[0])
max_ulica = ulice[0]
for i in ulice:
    if len(i) > max_slova:
        max_slova = len(i)
        max_ulica = i

print ("Broj ulica:", len(ulice))
print ("Najveci broj slova u ulici:", max_ulica)
