import numpy as np 
import cv2

def show(name, img):
    cv2.imshow(name, img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

slika = cv2.imread('slike/baboon.bmp')
show("Original", slika)

prva = slika[:, ::2].copy()
druga = slika[::2, :].copy()
treca = slika[::2, ::2].copy()

cv2.imwrite("Vertikalno_suzena.png", prva)
cv2.imwrite("Horizontalno_suzena.png", druga)
cv2.imwrite("Vertikalno_Horizontalno_suzena.png", treca)

show("Vertikalno_suzena", prva)
show("Horizontalno_suzena", druga)
show("Vertikalno_Horizontalno_suzena", treca)
