import numpy as np 
import cv2

slika = cv2.imread('slike/lenna.bmp')

crvena = slika.copy()
crvena[:,:,0:2] = 0

zelena = slika.copy()
zelena[:,:,::2] = 0

plava = slika.copy()
plava[:,:,1:3] = 0

cv2.imwrite("crvena.jpg", crvena)
cv2.imwrite("zelena.jpg", zelena)
cv2.imwrite("plava.jpg", plava)
