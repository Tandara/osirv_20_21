import numpy as np 
import cv2

def show(name, img):
    cv2.imshow(name, img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

slika = cv2.imread('slike/lenna.bmp')
show("Original", slika)

slika[:10,:] = 0
slika[-11:-1,:] = 0
slika[:,:10] = 0
slika[:,-11:-1] = 0

cv2.imwrite("Obrubljena_slika.jpg", slika)
show("Obrubljena_slika", slika)
