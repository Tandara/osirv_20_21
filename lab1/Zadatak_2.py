import numpy as np 
import cv2

def show(name, image):
    cv2.imshow(name, image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

slika = cv2.imread('slike/lenna.bmp')
show("Original", slika)

crvena = slika.copy()
crvena[:,:,0:2] = 0

zelena = slika.copy()
zelena[:,:,::2] = 0

plava = slika.copy()
plava[:,:,1:3] = 0

show("Crvena", crvena)
show("Zelena", zelena)
show("Plava", plava)
