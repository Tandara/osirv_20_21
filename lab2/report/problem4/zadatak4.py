import os
import cv2

from_path='lab2/slike'
to_path='lab2/report/problem4'

for threshold in [63, 127, 191]:
    for img_name in os.listdir(from_path):
        img=cv2.imread(os.path.join(from_path,img_name), cv2.IMREAD_GRAYSCALE) 
        cv2.imwrite(os.path.join(to_path,img_name.split('.')[0]+f"_{threshold}_threshold_value.jpg"),img * (img>threshold))
