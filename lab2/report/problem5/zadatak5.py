import numpy as np
import os
import cv2

to_path='lab2/report/problem5'

def quantisation (q, img):
    d = np.power(2, 8 - q)
    img = img.astype(np.float32)
    np.clip(img, 0, 255, img)
    img[:] = (np.floor(img / d) + 0.5) * d
    img = img.astype(np.uint8)
    cv2.imwrite(os.path.join(to_path,'boats_' + str(q) + '.bmp'), img)

img = cv2.imread("lab2/slike/BoatsColor.bmp", cv2.IMREAD_GRAYSCALE)

for i in range(1, 9):
    quantisation (i, img)
