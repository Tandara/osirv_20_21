import os
import cv2

from_path='lab2/slike'
to_path='lab2/report/problem3'

for img_name in os.listdir(from_path):
    img=cv2.imread(os.path.join(from_path,img_name), cv2.IMREAD_GRAYSCALE)
    img_inverted=cv2.bitwise_not(img)
    cv2.imwrite(os.path.join(to_path,img_name.split('.')[0]+"_invert.jpg"),img_inverted)
