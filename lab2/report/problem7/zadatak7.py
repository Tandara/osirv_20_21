import cv2
import numpy as np
from math import sin, cos, pi

img = cv2.imread('lab2/slike/baboon.bmp', cv2.IMREAD_GRAYSCALE)
rows,cols = img.shape

M = cv2.getRotationMatrix2D((cols/2,rows/2),30,0.25) 

copy_img = np.copy(img)

for i in range(30):
    copy_img = cv2.warpAffine(copy_img,M,(cols,rows))
    img = np.array([[d if d != 0 else c for c, d in zip(a, b)] for a, b in zip(img, copy_img)])

cv2.imwrite('lab2/report/problem7/babooon_rotated.bmp', img)
