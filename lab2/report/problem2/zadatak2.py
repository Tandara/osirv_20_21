import numpy as np
import cv2

def convolve(image, kernel):
    output = np.zeros( (image.shape[0] -kernel.shape[0] + 1,
                        image.shape[1] - kernel.shape[1] +1) )
    kernel_rev = kernel[::-1,::-1]


    for i in range(0, output.shape[0]):
        for j in range(0, output.shape[1]):
            for k in range(kernel.shape[0]):
                for l in range(kernel.shape[1]):
                    output[i,j] += image[i+k,j+l] * kernel_rev[k,l]

    output[output>255] = 255
    output[output<0]   = 0
    output = output.astype(np.uint8)
    return output

im = cv2.imread('lab2/slike/barbara.bmp', cv2.IMREAD_GRAYSCALE)

Identity = np.array([[0, 0, 0], [0, 1, 0], [0, 0, 0]], dtype = float)
img0 = convolve(im, Identity)
cv2.imwrite('Identity.png', img0)

Edge_detection_1 = np.array([[1, 0, -1], [0, 0, 0], [-1, 0, 1]], dtype = float)
img1 = convolve(im, Edge_detection_1)
cv2.imwrite('Edge_detection_1.png', img1)

Edge_detection_2 = np.array([[0, 1, 0], [1, -4, 1], [0, 1, 0]], dtype = float)
img2 = convolve(im, Edge_detection_2)
cv2.imwrite('Edge_detection_2.png', img2)

Edge_detection_3 = np.array([[-1, -1, -1], [-1, 8, -1], [-1, -1, -1]], dtype = float)
img3 = convolve(im, Edge_detection_3)
cv2.imwrite('Edge_detection_3.png', img3)

