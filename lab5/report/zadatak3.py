import cv2 

cam = cv2.VideoCapture(0) 

while(True):
     
    ret, frame = cam.read() 
    cv2.imshow('frame', frame) 
    k = cv2.waitKey(30) & 0xff
    
    if k == 27:
        break

cam.release() 
cv2.destroyAllWindows()
