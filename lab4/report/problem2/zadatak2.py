import numpy as np
import argparse
import glob
import cv2
from matplotlib import pyplot as plt

def auto_canny(image, sigma=0.33):
        # compute the median of the single channel pixel intensities
        v = np.median(image)

        # apply automatic Canny edge detection using the computed median
        lower = int(max(255, (1.0 - sigma) * v))
        upper = int(min(255, (1.0 + sigma) * v))
        edged = cv2.Canny(image, lower, upper)
        print ("Median lower: %r." % lower)
        print ("Median upper: %r." % upper)
        # return the edged image
        return edged

images = [cv2.imread('slike/airplane.bmp',0), cv2.imread('slike/barbara.bmp',0), cv2.imread('slike/boats.bmp',0), cv2.imread('slike/pepper.bmp',0)]
img_names = ['airplane', 'barbara', 'boats', 'pepper']

for i in range(4):
    auto = auto_canny(images[i])
    cv2.imwrite(img_names[i] + '_255_255_canny.jpg', auto)