import cv2
import sys
# Postavljanje trackera
tracker_types = ['MIL', 'KCF', 'BOOSTING', 'TLD', 'MEDIANFLOW']
tracker_type = tracker_types[0]

if tracker_type == 'MIL':
    tracker = cv2.TrackerMIL_create()
if tracker_type == 'KCF':
    tracker = cv2.TrackerKCF_create()
if tracker_type == 'BOOSTING':
    tracker = cv2.TrackerBoosting_create()
if tracker_type == 'TLD':
    tracker = cv2.TrackerTLD_create()
if tracker_type == 'MEDIANFLOW':
    tracker = cv2.TrackerMedianFlow_create()

# Ucitavanje videa
video = cv2.VideoCapture("test.mp4")

# Izlaz ako je ucitavanje neuspjesno
if not video.isOpened():
    print("Could not open video")
    sys.exit()

# Ucitavanje prvog framea
check, frame = video.read()
if not check:
    print('Cannot read video file')
    sys.exit()

# Definiranje pocetnog okvira
# bounding_box = (400, 60, 86, 100)
# Oznacavanje okvira na prvom frameu
bounding_box = cv2.selectROI(frame, False)

# Inicijalizacija trackera sa prvim frameom i oznacenim objektom
check = tracker.init(frame, bounding_box)

while True:
    # Citanje novog framea
    check, frame = video.read()
    if not check:
        break

    # Zapoceto odbrojavanje
    timer = cv2.getTickCount()

    # Azuriranje trackera
    check, bounding_box = tracker.update(frame)

    # Izracunavanje FPS-a
    fps = cv2.getTickFrequency() / (cv2.getTickCount() - timer);

    # Crtanje okvira
    if check:
        # Ako je pracenje uspjesno
        p1 = (int(bounding_box[0]), int(bounding_box[1]))
        p2 = (int(bounding_box[0] + bounding_box[2]), int(bounding_box[1] + bounding_box[3]))
        cv2.rectangle(frame, p1, p2, (0,0,255), 2, 1)
    else :
        # Ako je pracenje neuspjesno
        cv2.putText(frame, "Tracking failure detected", (50,80), cv2.FONT_HERSHEY_SIMPLEX, 0.75,(0,0,255),2)

    # Ispis metode na frameu
    cv2.putText(frame, tracker_type + " Tracker", (50,20), cv2.FONT_ITALIC,0.75, (1700,0,255),2);

    # Ispis FPS na frameu
    cv2.putText(frame, "FPS : " + str(int(fps)), (50,50), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (170,0,255), 2);

    # Prikaz rezultata
    cv2.imshow("Tracking", frame)

    # Izlaz pritiskom tipke ESC
    key_pres = cv2.waitKey(1) & 0xff
    if key_pres == 27 : break