import numpy as np
import cv2

boats = cv2.imread('slike/boats.bmp', 0)
airplane = cv2.imread('slike/airplane.bmp', 0)

def gaussian_noise(img, mu, sigma):
    out = img.astype(np.float32)
    noise = np.random.normal(mu, sigma, img.shape)
    out = out + noise
    out[out<0] = 0
    out[out>255] = 255
    return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
    out = img.astype(np.float32)
    limit = ((float(percent)/2.0)/100.0) * 255.0
    noise = np.random.uniform(0,255, img.shape)
    out[noise<limit] = 0
    out[noise>(255-limit)] = 255
    out[out>255] = 255
    out[out<0] = 0
    return out.astype(np.uint8)

def custom_filter(image, radius):
    image_copy = image
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            image_copy[i,j] = np.median(image_copy[i:i+radius, j:j+radius])
    return image_copy

percents = [1, 10]
sigmas = [5, 15, 35]
neighbors = [3, 5, 7, 11]
gaussian_kernel_sizes = [3, 5, 7]



for median in neighbors:
    for sigma in sigmas:
        cv2.imwrite('boats_gaussian_sigma_' + str(sigma) + '_median_' + str(median) + '.jpg', cv2.medianBlur(gaussian_noise(boats, 0, sigma), median))
        cv2.imwrite('airplane_gaussian_sigma_' + str(sigma) + '_median_' + str(median) + '.jpg', cv2.medianBlur(gaussian_noise(airplane, 0, sigma), median))

    for percent in percents:
        cv2.imwrite('boats_salt_and_pepper_' + str(percent) + '_median_' + str(median) + '.jpg', cv2.medianBlur(salt_n_pepper_noise(boats, percent), median))
        cv2.imwrite('airplane_salt_and_pepper_' + str(percent) + '_median_' + str(median) + '.jpg', cv2.medianBlur(salt_n_pepper_noise(airplane, percent), median))
        
for kernel in gaussian_kernel_sizes:
    for blurSigma in sigmas:
        for sigma in sigmas:
            cv2.imwrite('boats_gaussian_sigma_' + str(sigma) + '_gaussian_' + str(kernel) + '_' + str(blurSigma) + '.jpg', cv2.GaussianBlur(gaussian_noise(boats, 0, sigma), (kernel, kernel), blurSigma))
            cv2.imwrite('airplane_gaussian_sigma_' + str(sigma) + '_gaussian_' + str(kernel) + '_' + str(blurSigma) + '.jpg', cv2.GaussianBlur(gaussian_noise(airplane, 0, sigma), (kernel, kernel), blurSigma))
        
        for percent in percents:
            cv2.imwrite('boats_salt_and_pepper_' + str(percent) + '_gaussian_' + str(kernel) + '_' + str(blurSigma) + '.jpg', cv2.GaussianBlur(salt_n_pepper_noise(boats, percent), (kernel, kernel), blurSigma))
            cv2.imwrite('airplanes_salt_and_pepper_' + str(percent) + '_gaussian_' + str(kernel) + '_' + str(blurSigma) + '.jpg', cv2.GaussianBlur(salt_n_pepper_noise(airplane, percent), (kernel, kernel), blurSigma))

img_salt = salt_n_pepper_noise(boats, 20)
cv2.imwrite('Original_boats.jpg', boats)
cv2.imwrite('OpenCV_boats.jpg', cv2.medianBlur(img_salt,3))
cv2.imwrite('Custom_boats.jpg', custom_filter(boats,3))


