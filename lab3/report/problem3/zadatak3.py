import cv2

boats = cv2.imread('slike/boats.bmp', 0)
baboon = cv2.imread('slike/baboon.bmp', 0)
airplane = cv2.imread('slike/airplane.bmp', 0)
images = [boats, baboon, airplane]
image_names = ['boats', 'baboon', 'airplane']
thresholds = [50, 120, 180]

for i in range(len(images)):
    for threshold in thresholds:
        ret,thresh1 = cv2.threshold(images[i],threshold,255,cv2.THRESH_BINARY)
        cv2.imwrite(image_names[i] + '_binary_threshold_' + str(threshold) + '.jpg', thresh1)
        ret,thresh2 = cv2.threshold(images[i],threshold,255,cv2.THRESH_BINARY_INV)
        cv2.imwrite(image_names[i] + '_binary_inv_threshold_' + str(threshold) + '.jpg', thresh2)
        ret,thresh3 = cv2.threshold(images[i],threshold,255,cv2.THRESH_TRUNC)
        cv2.imwrite(image_names[i] + '_trunc_threshold_' + str(threshold) + '.jpg', thresh3)
        ret,thresh4 = cv2.threshold(images[i],threshold,255,cv2.THRESH_TOZERO)
        cv2.imwrite(image_names[i] + '_tozero_threshold_' + str(threshold) + '.jpg', thresh4)
        ret,thresh5 = cv2.threshold(images[i],threshold,255,cv2.THRESH_TOZERO_INV)
        cv2.imwrite(image_names[i] + '_tozero_inv_threshold_' + str(threshold) + '.jpg', thresh5)
    thresh6 = cv2.adaptiveThreshold(images[i],255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,11,2)
    cv2.imwrite(image_names[i] + '_adaptive_mean_threshold_' + str(threshold) + '.jpg', thresh6)
    thresh7 = cv2.adaptiveThreshold(images[i],255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
    cv2.imwrite(image_names[i] + '_adaptive_gaussian_' + str(threshold) + '.jpg', thresh7)
    ret,thresh8 = cv2.threshold(images[i],0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    cv2.imwrite(image_names[i] + '_otsu_' + str(threshold) + '.jpg', thresh8)
    print(ret)
    
