import numpy as np
import cv2
import matplotlib.pyplot as plt

img = cv2.imread('slike/airplane.bmp', 0)

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def uniform_noise(img, low, high):
    out = img.astype(np.float32)
    noise = np.random.uniform(low,high, img.shape)
    out = out + noise
    out[out>255] = 255
    out[out<0] = 0
    return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def savehist(filename, img):
  hist,bins = np.histogram(img.flatten(), bins=256, range=(0,255))
  plt.vlines(np.arange(len(hist)), 0, hist)
  plt.savefig("report/problem1/" + filename)

cv2.imwrite("report/problem1/airplane_gaussian_noise_15.jpg", gaussian_noise(img, 0, 15))
cv2.imwrite("report/problem1/airplane_gaussian_noise_25.jpg", gaussian_noise(img, 0, 25))
cv2.imwrite("report/problem1/airplane_gaussian_noise_40.jpg", gaussian_noise(img, 0, 40))
cv2.imwrite("report/problem1/airplane_uniform_noise_20.jpg", uniform_noise(img, -20, 20))
cv2.imwrite("report/problem1/airplane_uniform_noise_40.jpg", uniform_noise(img, -40, 40))
cv2.imwrite("report/problem1/airplane_uniform_noise_60.jpg", uniform_noise(img, -60, 60))
cv2.imwrite("report/problem1/airplane_salt_n_pepper_noise_5.jpg", salt_n_pepper_noise(img, 5))
cv2.imwrite("report/problem1/airplane_salt_n_pepper_noise_10.jpg", salt_n_pepper_noise(img, 10))
cv2.imwrite("report/problem1/airplane_salt_n_pepper_noise_15.jpg", salt_n_pepper_noise(img, 15))
cv2.imwrite("report/problem1/airplane_salt_n_pepper_noise_20.jpg", salt_n_pepper_noise(img, 20))

savehist("histogram_airplane_gaussian_noise_15.jpg", gaussian_noise(img, 0, 15))
savehist("histogram_airplane_gaussian_noise_25.jpg", gaussian_noise(img, 0, 25))
savehist("histogram_airplane_gaussian_noise_40.jpg", gaussian_noise(img, 0, 40))
savehist("histogram_airplane_uniform_noise_20.jpg", uniform_noise(img, -20, 20))
savehist("histogram_airplane_uniform_noise_40.jpg", uniform_noise(img, -40, 40))
savehist("histogram_airplane_uniform_noise_60.jpg", uniform_noise(img, -60, 60))
savehist("histogram_airplane_salt_n_pepper_noise_5.jpg", salt_n_pepper_noise(img, 5))
savehist("histogram_airplane_salt_n_pepper_noise_10.jpg", salt_n_pepper_noise(img, 10))
savehist("histogram_airplane_salt_n_pepper_noise_15.jpg", salt_n_pepper_noise(img, 15))
savehist("histogram_airplane_salt_n_pepper_noise_20.jpg", salt_n_pepper_noise(img, 20))




